1. Apakah perbedaan antara JSON dan XML?

Jawaban:

Perbedaan pengertian:

- JSON adalah standar teks untuk melakukan pertukaran data.

- XML adalah format software-hardware untuk pertukaran data.

Kompleksitas:
 - JSON lebih sederhana dan mudah dibaca.'

 - XML lebih rumit darippada JSON

 Orientasi:

 - JSON berorientasi kepada data.

 - XML berorientasi kepada dokumen.

 Array:

 - JSON bisa mengguakan array.

 - XML tidak mendukung array.

 Ekstensi file:

 - File JSON memiliki ekstensi .json

 - FIle XML diakhiri dengan ekstensi .xml

2. Apakah perbedaan antara HTML dan XML?

Kepanjangan:

- XML adalah eXtensible Markup Language.

- HTML adalah singkatan dari Hypertext Markup Language.

Fokus penyajian:

- XML berfokus pada transfer data

- HTML berfokus kepada penyajian data

Sensitivitas:

- XML itu case sensitive

- HTML itu tidak case sensitive

Namepace:

- XML menyediakan dukungan namespace

- HTML tidak menyediakan dukungan namespace

Tag penutup:

- XML strict untuk menggunakan tag penutup

- HTML tidak strict untuk menggunakan tag penutup

Referensi:

- https://blogs.masterweb.com/perbedaan-xml-dan-html/

- https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html


