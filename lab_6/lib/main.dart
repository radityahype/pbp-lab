import 'package:flutter/material.dart';
import 'LandingPage/LandingPage.dart';
import 'Navbar/Navbar.dart';
import 'package:lab_6/screen/drawer.dart';
import 'package:lab_6/widgets/card.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daily Journal',
      theme: ThemeData(primarySwatch: Colors.indigo, fontFamily: "Montserrat"),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          // gradient: LinearGradient(
          //     begin: Alignment.centerLeft,
          //     end: Alignment.centerRight,
          //     colors: [
          //       Color.fromRGBO(195, 20, 50, 1.0),
          //       Color.fromRGBO(36, 11, 54, 1.0)
          //     ]),
          color: Color.fromRGBO(6, 12, 35, 1) 
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 40.0),
                child: LandingPage(),
              ),
              Text(
                "Daftar Jurnal",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                ),
              ),
              CardDek(),
            ],
          ),
        ),
      ),
    );
  }
}