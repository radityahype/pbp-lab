from django.shortcuts import render
from .models import Note as note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    message = note.objects.all().values()
    response = {'pesan': message}
    return render(request, 'lab2.html', response)

def XML(request):
    data = serializers.serialize('xml', note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def JSON(request):
    data = serializers.serialize('json', note.objects.all())
    return HttpResponse(data, content_type="application/json")