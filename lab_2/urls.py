from django.urls import path
from .views import JSON, XML, index

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('xml', XML, name='xml'),
    path('json', JSON, name='json'),
]