from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=20)
    From = models.CharField(max_length=20)
    title = models.CharField(max_length=20, default="hi")
    message = models.CharField(max_length=100)
