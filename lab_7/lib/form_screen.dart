import 'package:flutter/material.dart';
import 'LandingPage/LandingPage.dart';
import 'Navbar/Navbar.dart';
import 'package:lab_7/screen/drawer.dart';
import 'package:lab_7/form_screen.dart';
import 'package:date_field/date_field.dart';

class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
  const FormScreen({Key? key}) : super(key: key);
}

class FormScreenState extends State<FormScreen> {
  String? _hari;
  String? _tanggal;
  String? _demam;
  String? _batuk;
  String? _kelelahan;
  String? _kehilanganPenciuman;
  String? _curhat;


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  Widget _buildHari() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Hari ke- :', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 2,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Hari is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _hari = value!;
      },
    );
  }

  Widget _buildTanggal() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal :', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tanggal is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _tanggal = value!;
      },
    );
  }

  Widget _buildDemam() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Demam : (Yes/No)', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 3,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Demam is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _demam = value!;
      },
    );
  }
  
  Widget _buildBatuk() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Batuk : (Yes/No)', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 3,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Batuk is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _batuk = value!;
      },
    );
  }

  Widget _buildKelelahan() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Kelelahan : (Yes/No)', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 3,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Kelelahan is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _kelelahan = value!;
      },
    );
  }

  Widget _buildKehilanganPenciuman() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Kelelahan : (Yes/No)', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 3,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Kelihangan Penciuman is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _kehilanganPenciuman = value!;
      },
    );
  }

  Widget _buildCurhat() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Ceritakan apa yang anda rasakan hari ini: ', fillColor: Colors.cyan, filled: true),
      style: TextStyle(color: Colors.white, fontSize: 22),

      maxLength: 100,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Curhat harus diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        _curhat = value!;
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1) 
        ),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Navbar(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 40.0),
                  child: LandingPage(),
                ),
                _buildHari(),
                _buildTanggal(),
                _buildDemam(),
                _buildBatuk(),
                _buildKelelahan(),
                _buildKehilanganPenciuman(),
                _buildCurhat(),

                SizedBox(
                  height: 50),
                ElevatedButton(
                  child: Text(
                    'Tambah Jurnal',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }

                    _formKey.currentState!.save();

                    print(_hari);
                    print(_tanggal);
                    print(_demam);
                    print(_batuk);
                    print(_kelelahan);
                    print(_kehilanganPenciuman);                    //Send to API                    //Send to API                    //Send to API                    //Send to API
                    print(_curhat);                    //Send to API                    //Send to API                    //Send to API
                                        //Send to API
                                        //Send to API
                  },
                ),
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}