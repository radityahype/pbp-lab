/// Flutter code sample for Card

// This sample shows creation of a [Card] widget that can be tapped. When
// tapped this [Card]'s [InkWell] displays an "ink splash" that fills the
// entire card.

import 'package:flutter/material.dart';


class CardDek extends StatefulWidget {
  const CardDek({Key? key}) : super(key: key);

  @override
  _CardDekState createState() => _CardDekState();
}

class _CardDekState extends State<CardDek> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(20.0),
                child:
                  Column(
                    children: [
                      SizedBox(height: 5,),
                      Text('Hari ke: 1',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      textAlign: TextAlign.center,),
                      SizedBox(height: 5,),
                      Text('Tanggal Nov. 1, 2021',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      textAlign: TextAlign.center,),
                      SizedBox(height: 20,),
                      Text("Demam: False\n Batuk: False\n Kelelahan: True\n Kehilangan Penciuman: False\n Keluhan: gpp", style: TextStyle(
                        fontSize: 14,
                      ),),
                      SizedBox(height: 20,),
                    ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
