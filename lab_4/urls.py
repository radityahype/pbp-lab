from django.urls import path

from lab_3.views import add_friend
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index_lab4'),
    # TODO Add friends path using friend_list Views
    path('add-note', add_note, name="add-note"),
    path('note-list', note_list, name="note-list")
]